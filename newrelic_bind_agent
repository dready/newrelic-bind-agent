#! /usr/bin/env ruby

#
# An agent which extracts BIND statistics and report them to New Relic.
#

require "rubygems"
require "bundler/setup"

require "newrelic_plugin"
require "socket"
require 'nokogiri'

module BindAgent

  class Agent < NewRelic::Plugin::Agent::Base

    agent_guid "au.com.fairfax.bind_agent"
    agent_version "1.0.0"
    agent_config_options :stats_url  # bind statistics channel url to v2 xml
    agent_human_labels("BIND Agent") { Socket.gethostname }

    def poll_cycle
      #puts "fetching " + stats_url
      uri = URI.parse(stats_url)
      response = Net::HTTP.get_response(uri)
      doc = Nokogiri::XML(response.body)
      #doc = Nokogiri::XML(open('bindstats-kumo1.xml'))
      #puts "got response " + response.code

      doc.xpath('/isc/bind/statistics/server/requests/opcode/name').each do |node|
        metric = "Request/Opcode/#{node.text}"
        val = node.next_element.text.to_i
        puts "#{metric} = #{val}"
        report_metric metric, "Value", val
      end
      doc.xpath('/isc/bind/statistics/server/queries-in/rdtype/name').each do |node|
        metric = "Request/RDType/#{node.text}"
        val = node.next_element.text.to_i
        puts "#{metric} = #{val}"
        report_metric metric, "Value", val
      end
      doc.xpath('/isc/bind/statistics/server/nsstat/name').each do |node|
        metric = "NSStat/#{node.text}"
        val = node.next_element.text.to_i
        puts "#{metric} = #{val}"
        report_metric metric, "Value", val
      end
      doc.xpath('/isc/bind/statistics/server/zonestat/name').each do |node|
        metric = "ZoneStat/#{node.text}"
        val = node.next_element.text.to_i
        puts "#{metric} = #{val}"
        report_metric metric, "Value", val
      end
      doc.xpath('/isc/bind/statistics/memory/summary/*').each do |node|
        metric = "Mem/#{node.name}"
        puts "#{metric} = #{node.text.to_i}"
        report_metric metric, "Value", node.text.to_i
      end
    end

  end

  #
  # Register this agent with the component.
  # The BindAgent is the name of the module that defines this
  # driver (the module must contain at least three classes - a
  # PollCycle, a Metric and an Agent class, as defined above).
  #
  NewRelic::Plugin::Setup.install_agent :bind, BindAgent

  #
  # Launch the agent; this never returns.
  #
  NewRelic::Plugin::Run.setup_and_run

end
